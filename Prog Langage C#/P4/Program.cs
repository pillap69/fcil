﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tabPrenom = new string[5];

            tabPrenom[0] = "José";
            tabPrenom[1] = "Yassin";
            tabPrenom[2] = "Philippe";
            tabPrenom[3] = "Léa";
            tabPrenom[4] = "Mywin";

            string[] tabClassement = new string[5];

            tabClassement[0] = "premier";
            tabClassement[1] = "deuxième";
            tabClassement[2] = "troisième";
            tabClassement[3] = "quatrième";
            tabClassement[4] = "cimquième";

            for(int x = 0; x < tabPrenom.Length; x++)
            {
                Console.WriteLine("le " + tabClassement[x] + " prénom est : " + tabPrenom[x]);
            }

            Console.ReadKey();
        }
    }
}
