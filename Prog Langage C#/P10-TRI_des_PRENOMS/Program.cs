﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P10_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tabPrenom = new 
                string[5] { "Philippe", "ören", "éric", "Jean-Pierre", "Jeani" };

            Dictionary<char, char> odicoLettreAccent;
            odicoLettreAccent = new Dictionary<char, char>();

            odicoLettreAccent.Add('é', 'e');
            odicoLettreAccent.Add('è', 'e');
            odicoLettreAccent.Add('ê', 'e');
            odicoLettreAccent.Add('ö', 'o');

            Boolean estRentre = true;

            
            for (int x = 0; x < tabPrenom.Length && estRentre ; x++)
            {
                estRentre = false;
                for(int z = 0; z < tabPrenom.Length - 1; z++)
                {
                    int longueur1 = tabPrenom[z].Length;
                    int longueur2 = tabPrenom[z + 1].Length;

                    string prenom1 = tabPrenom[z].ToLower();
                    string prenom2 = tabPrenom[z + 1].ToLower();

                    prenom1 = prenom1.Replace("-", string.Empty);
                    prenom2 = prenom2.Replace("-", string.Empty);

                    if (prenom1 == prenom2) // Suis dans le cas ou l'égalité est possible
                    {
                        continue;
                    }

                    if (longueur1 < longueur2)
                    {
                        

                        for (int m=0; m < longueur1; m++)
                        {
                            char lettreSansAccent;
                            char lettrePrenom1,lettrePrenom2;

                            /*if(odicoLettreAccent.TryGetValue(prenom1[m], out lettreSansAccent))
                            {
                                lettrePrenom1 = lettreSansAccent;

                            }
                            else
                            {
                                lettrePrenom1 = prenom1[m];
                            }*/
                            try
                            {
                                lettrePrenom1 = odicoLettreAccent[prenom1[m]];
                            }
                            catch(Exception ex)
                            {
                                //Console.WriteLine(ex.Message);
                                lettrePrenom1 = prenom1[m];
                            }

                            /*if (odicoLettreAccent.TryGetValue(prenom2[m], out lettreSansAccent))
                            {
                                lettrePrenom2 = lettreSansAccent;

                            }
                            else
                            {
                                lettrePrenom2 = prenom2[m];
                            }*/

                            try
                            {
                                lettrePrenom2 = odicoLettreAccent[prenom2[m]];
                            }
                            catch (Exception ex)
                            {
                                //Console.WriteLine(ex.Message);
                                lettrePrenom2 = prenom2[m];
                            }

                            if (lettrePrenom1 > lettrePrenom2)
                            {
                                permutation(tabPrenom, z);
                                estRentre = true;
                                break;

                            }
                            else
                            {
                                if (lettrePrenom1 < lettrePrenom2)
                                {
                                    break;
                                }
                        
                            }

                        }

                    }
                    else
                    {

                        for (int m = 0; m < longueur2 ; m++)
                        {
                            char lettreSansAccent;
                            char lettrePrenom1, lettrePrenom2;

                            if (odicoLettreAccent.TryGetValue(prenom1[m], out lettreSansAccent))
                            {
                                lettrePrenom1 = lettreSansAccent;

                            }
                            else
                            {
                                lettrePrenom1 = prenom1[m];
                            }

                            if (odicoLettreAccent.TryGetValue(prenom2[m], out lettreSansAccent))
                            {
                                lettrePrenom2 = lettreSansAccent;

                            }
                            else
                            {
                                lettrePrenom2 = prenom2[m];
                            }



                            if (lettrePrenom1 > lettrePrenom2)
                            {
                                permutation(tabPrenom, z);
                                estRentre = true;
                                break;

                            }
                            else
                            {
                                if(lettrePrenom1 == lettrePrenom2)
                                {
                                    continue;
                                }
                                else
                                {
                                    break; // cas ou inférieur

                                }

                            }


                        }

                    }
                }

            }

            // Affichage tableau trié
            int t = 0;
            while(t < tabPrenom.Length)
            {
                Console.WriteLine(tabPrenom[t]);
                t++;
            }

            Console.ReadKey();

        }

        // Procédure qui permute les prénoms
        static void permutation(string[] stabPrenom,int snum)
        {
            string stringTemp = stabPrenom[snum];
            stabPrenom[snum] = stabPrenom[snum + 1];
            stabPrenom[snum + 1] = stringTemp;


        }
    }
}
