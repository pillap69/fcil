﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {

            string maPhrase;
            maPhrase = "Il fait gris dehors !";

            int longueurChaine = maPhrase.Length;

            Console.WriteLine("La longueur de la chaine est : {0} ", maPhrase.Length);

            //déclaration du tableau
            string[] tabChaine;
            // Création - Instanciation du tableau 
            tabChaine = new string[6];
            // peuplement du tableau
            tabChaine[0] = "Philippe";
            tabChaine[1] = "Léa";
            tabChaine[2] = "Yassin";
            tabChaine[3] = "Adam";
            tabChaine[4] = "Mywin";
            tabChaine[5] = "Dylan";

            //affichage du tableau
            Console.WriteLine("Affichage du tableau de prénom :");
            for(int x = 0; x < tabChaine.Length; x++)
            {

                Console.WriteLine(tabChaine[x]);


            }

            Console.WriteLine("Affichage de ma chaine caractère par caractère :");
            for (int x = 0; x < maPhrase.Length; x++)
            {

                Console.WriteLine(maPhrase[x]);


            }

            char caractere =  maPhrase[1];

            Console.WriteLine("Affichage du char qui est en deuxième position dans le tableau : " + caractere);

            tabChaine[3]= Convert.ToString('Z');

            string partie1 = maPhrase.Substring(0, 7);
            string partie2 = maPhrase.Substring(partie1.Length + 1, maPhrase.Length - (partie1.Length + 1)); // + 1 pour l'espace
            string nouvelleChaine = partie1 + " pas " + partie2;
            Console.WriteLine("La nouvelle chaine : " + nouvelleChaine);

            Console.ReadKey();

        }
    }
}
