﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace P14_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {

            // Code permettant de récupérer les données dans un tableau

            // récupération de l'objet de type MySqlDataReader

            string query = "select nom,prenom from visiteur order by nom ASC";
            MySqlDataReader ord = getReader(query);

            // Ici déclarer et instancier un tableau de tableau tabVisiteurs[][]
            List<string[]> olistTabVisiteur = new List<string[]>();
            // Boucle de lecture du paquet de données
            int x = 0;
            while (ord.Read())
            {
                //ici instancier un tableau de deux places nommé tabVisiteur[] qui contientront les nom et prénom du visiteur médical
                string[] tabVisiteur = new string[2];
                // Ici mettre les données dans le tableau
                tabVisiteur[0] = Convert.ToString(ord["nom"]);
                tabVisiteur[1] = Convert.ToString(ord["prenom"]);

                // Ici mettre les données dans le tableau de tableau
                olistTabVisiteur.Add(tabVisiteur);
                /*tabVisiteurs[x][0] = tabVisiteur[0];
                tabVisiteurs[x][1] = tabVisiteur[1];*/
                x++;
            }

            x = 0;
            int longMaxNom = 0;
            while (x < olistTabVisiteur.Count)
            {
                if (longMaxNom < olistTabVisiteur[x][0].Length)
                {
                    longMaxNom = olistTabVisiteur[x][0].Length;
                }

                x++;
            }


            // Ici intégrer le code permettant d'afficher l'ensemble des noms et prénoms.
            x = 0;
            while (x < olistTabVisiteur.Count)
            {
                int nbEspace = longMaxNom - olistTabVisiteur[x][0].Length;
                string espace = null;

                for (int i = 0; i < nbEspace; i++)
                {
                    espace = espace + " ";

                }
                Console.WriteLine(olistTabVisiteur[x][0] + espace + "\t\t\t" + olistTabVisiteur[x][1]);
                x++;
            }

            Console.ReadKey();

        }

        static MySqlDataReader getReader(string squery)
        {
            string connectionString = "SERVER=127.0.0.1; DATABASE=gsb; UID=root; PASSWORD=";
            MySql.Data.MySqlClient.MySqlConnection ocnx = new MySqlConnection(connectionString);
            ocnx.Open();
            MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
            MySqlDataReader ord = ocmd.ExecuteReader();
            return ord;
        }
    }
}
