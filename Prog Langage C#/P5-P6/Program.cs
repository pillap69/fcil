﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P5_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {
            //déclaration du tableau et instanciation du tableau
            int[] tabEntier;
            tabEntier = new int[5];

            /*tabEntier[0] = 50;
              tabEntier[1] = 500;
              tabEntier[2] = 25;
              tabEntier[3] = 501;
              tabEntier[4] = 75;*/

            // Déclaration et instanciation d'un objet de type random
            Random ord = new Random();

            for (int x = 0; x < tabEntier.Length; x++)
            {
                tabEntier[x] = ord.Next(-10000, 10000);

            }

            Console.WriteLine("Les nombres aléatoires générés sont : ");

            for (int x = 0; x < tabEntier.Length; x++)
            {
                Console.WriteLine(tabEntier[x]);

            }


            int nbMax = tabEntier[0];

            for(int x = 1; x < tabEntier.Length; x++)
            {
                 if(tabEntier[x] > nbMax){

                    nbMax = tabEntier[x];

                }

            }

             
            Console.WriteLine(" \r\n Le nombre maximal est : " + nbMax);
            Console.ReadKey();



        }
    }
}
