﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using P15_FCIL.mesClasses;

namespace P16_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {

            // Code permettant de récupérer les données dans un tableau

            // récupération de l'objet de type MySqlDataReader

            string query = "select id,nom,prenom,login,mdp,adresse from visiteur order by nom ASC";
            MySqlDataReader ord = getReader(query);

            // Ici déclarer et instancier une collection de type List
            List<Cvisiteur> olistTabVisiteur = new List<Cvisiteur>();
            // Boucle de lecture du paquet de données
            int x = 0;
            while (ord.Read())
            {
                //ici instancier un objet de type Cvisiteur;
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]),Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]));

                
                // Ici mettre les données dans la collection
                olistTabVisiteur.Add(ovisiteur);

                x++;
            }

            // Calcul des longueurs maximales des différents attribut
            x = 0;
            int longMaxNom = 0;
            while (x < olistTabVisiteur.Count)
            {
                if (longMaxNom < olistTabVisiteur[x].nom.Length)
                {
                    longMaxNom = olistTabVisiteur[x].nom.Length;
                }

                x++;
            }

            x = 0;
            int longMaxPrenom = 0;
            while (x < olistTabVisiteur.Count)
            {
                if (longMaxPrenom < olistTabVisiteur[x].prenom.Length)
                {
                    longMaxPrenom = olistTabVisiteur[x].prenom.Length;
                }

                x++;
            }
              
            x = 0;
            int longMaxLogin = 0;
            while (x < olistTabVisiteur.Count)
            {
                if (longMaxLogin < olistTabVisiteur[x].login.Length)
                {
                    longMaxLogin = olistTabVisiteur[x].login.Length;
                }

                x++;
            }


            // Ici intégrer le code permettant d'afficher l'ensemble des attributs.
            x = 0;
            while (x < olistTabVisiteur.Count)
            {

                Console.WriteLine(olistTabVisiteur[x].id + "\t" + olistTabVisiteur[x].nom + getEspace(longMaxNom, olistTabVisiteur[x].nom.Length) + "\t" + olistTabVisiteur[x].prenom + getEspace(longMaxPrenom, olistTabVisiteur[x].prenom.Length) + "\t" + olistTabVisiteur[x].login + getEspace(longMaxLogin, olistTabVisiteur[x].login .Length) +  "\t" + olistTabVisiteur[x].mdp + "\t" + olistTabVisiteur[x].adresse);
                x++;
            }

            Console.ReadKey();

        }

        //fonction qui calcule le nombre d'espace
        static string getEspace(int slgMotPlusLong, int slgMotAutre)
        {
            int nbEspace =  slgMotPlusLong - slgMotAutre;
            string espace = null;

            for (int i = 0; i < nbEspace; i++)
            {
                espace = espace + " ";

            }

            return espace;
        }

        static MySqlDataReader getReader(string squery)
        {
            string connectionString = "SERVER=127.0.0.1; DATABASE=gsb; UID=root; PASSWORD=";
            MySql.Data.MySqlClient.MySqlConnection ocnx = new MySqlConnection(connectionString);
            ocnx.Open();
            MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
            MySqlDataReader ord = ocmd.ExecuteReader();
            return ord;
        }
    }

}
