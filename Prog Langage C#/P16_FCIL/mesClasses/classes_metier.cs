﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P15_FCIL.mesClasses
{
    public class Cvisiteur
    {
        public string id;
        public string nom;
        public string prenom;
        public string login;
        public string mdp;
        public string adresse;
        public Cvisiteur(string sid,string snom, string sprenom,string slogin, string smdp, string sadresse)
        {
            id = sid;
            nom = snom;
            prenom = sprenom;
            login = slogin;
            mdp = smdp;
            adresse = sadresse;

        }

    }
}
