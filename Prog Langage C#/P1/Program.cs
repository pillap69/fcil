﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P1_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Travail à faire : compléter les zones en commentaire */

            int z;
            z = 5 / 3;
            System.Console.WriteLine("La valeur de l'entier est : " + z);

            double y = 5 / 3d;
            Console.WriteLine("La valeur du réel double est : " + y);

            Single v = 5 / 3f;
            Console.WriteLine("La valeur du réel simple est : " + v);

            decimal x = 5 / 3m;

            Console.WriteLine("La valeur du réel décimal est : " + x);


            Console.WriteLine("---------------------------------------------------");



            int t = int.MaxValue;
            
            Console.WriteLine("La valeur maximale d'un entier :" + t);

            int w = int.MinValue;

            Console.WriteLine("La valeur minimale d'un entier :" + w);

            double a = double.MaxValue;
            Console.WriteLine("La valeur maximale d'un réel double :" + a);

            double g = double.MinValue;
            Console.WriteLine("La valeur minimale d'un réel double :" + g);

            //.............
            decimal q = decimal.MaxValue;
            Console.WriteLine("La valeur maximale d'un réel décimal :" + q);

            decimal l = decimal.MinValue;
            Console.WriteLine("La valeur maximale d'un réel décimal :" + l);

            //.............

            Console.ReadKey();

        }
    }
}
