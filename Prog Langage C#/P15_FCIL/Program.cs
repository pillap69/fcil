﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using P15_FCIL.mesClasses;

namespace P15_FCIL
{
    class Program
    {
        static void Main(string[] args)
        {

            // Code permettant de récupérer les données dans un tableau

            // récupération de l'objet de type MySqlDataReader

            string query = "select nom,prenom from visiteur order by nom ASC";
            MySqlDataReader ord = getReader(query);

            // Ici déclarer et instancier une collection de type List
            List<Cvisiteur> olistTabVisiteur = new List<Cvisiteur>();
            // Boucle de lecture du paquet de données
            int x = 0;
            while (ord.Read())
            {
                //ici instancier un objet de type Cvisiteur;
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]));
  

                // Ici mettre les données dans la collection
                olistTabVisiteur.Add(ovisiteur);

                x++;
            }

            x = 0;
            int longMaxNom = 0;
            while (x < olistTabVisiteur.Count)
            {
                if (longMaxNom < olistTabVisiteur[x].nom.Length)
                {
                    longMaxNom = olistTabVisiteur[x].nom.Length;
                }

                x++;
            }


            // Ici intégrer le code permettant d'afficher l'ensemble des noms et prénoms.
            x = 0;
            while (x < olistTabVisiteur.Count)
            {
                int nbEspace = longMaxNom - olistTabVisiteur[x].nom.Length;
                string espace = null;

                for (int i = 0; i < nbEspace; i++)
                {
                    espace = espace + " ";

                }
                Console.WriteLine(olistTabVisiteur[x].nom + espace + "\t\t\t" + olistTabVisiteur[x].prenom);
                x++;
            }

            Console.ReadKey();

        }

        static MySqlDataReader getReader(string squery)
        {
            string connectionString = "SERVER=127.0.0.1; DATABASE=gsb; UID=root; PASSWORD=";
            MySql.Data.MySqlClient.MySqlConnection ocnx = new MySqlConnection(connectionString);
            ocnx.Open();
            MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
            MySqlDataReader ord = ocmd.ExecuteReader();
            return ord;
        }
    }

}

